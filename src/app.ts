import express, { Express } from 'express'
import { Server } from 'http'
import { UserController } from './user/user.controller'
import { ILogger } from './logger/logger.interface'
import { inject, injectable } from 'inversify'
import { TYPES } from './types'
import 'reflect-metadata'
import { IExceptionFilter } from './errors/exception.filter.interface'
import { json } from 'body-parser'
import { IConfigService } from './config/config.service.interface'
import { PrismaService } from './database/prisma.service'
import { UserMiddleware } from './common/user.middleware'

@injectable()
export class App {
  app: Express
  server: Server
  port: number

  constructor(
    @inject(TYPES.Logger) private logger: ILogger,
    @inject(TYPES.UserController) private userController: UserController,
    @inject(TYPES.ExceptionFilter) private exceptionFilter: IExceptionFilter,
    @inject(TYPES.ConfigService) private configService: IConfigService,
    @inject(TYPES.PrismaService) private prismaService: PrismaService,
  ) {
    this.app = express()
    this.port = 8000
  }

  useRoutes(): void {
    this.app.use('/users', this.userController.router)
  }

  useExceptionFilters(): void {
    this.app.use(this.exceptionFilter.catch.bind(this.exceptionFilter))
  }

  useMiddleware(): void {
    this.app.use(json())

    const userMiddleware = new UserMiddleware(this.configService.get('JWT_SECRET'))

    this.app.use(userMiddleware.execute.bind(userMiddleware))
  }

  public async init(): Promise<void> {
    this.useMiddleware()
    this.useRoutes()
    this.useExceptionFilters()
    await this.prismaService.connect()
    this.server = this.app.listen(this.port)
    this.logger.log(`Server is running on http://localhost:${this.port}`)
  }
}
