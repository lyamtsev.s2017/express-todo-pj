import { IMiddleware } from './middleware.interface'
import { NextFunction, Request, Response } from 'express'

export class AuthMiddleware implements IMiddleware {
  execute(req: Request, res: Response, next: NextFunction): void {
    const user = req.user
    if (user) {
      return next()
    }

    res.status(401).send({ error: 'Unauthorized' })
  }
}
