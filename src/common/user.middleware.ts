import { IMiddleware } from './middleware.interface'
import { NextFunction, Request, Response } from 'express'
import { verify } from 'jsonwebtoken'

export class UserMiddleware implements IMiddleware {
  constructor(private secret: string) {}

  execute(req: Request, res: Response, next: NextFunction): void {
    const authHeader = req.headers.authorization
    if (authHeader) {
      const token = authHeader.split(' ')[1]
      verify(token, this.secret, (err, payload) => {
        if (err) {
          next()
        } else if (payload && typeof payload !== 'string') {
          req.user = payload.email
          next()
        }
      })
    } else {
      next()
    }
  }
}
