import { IConfigService } from './config.service.interface'
import { config, DotenvConfigOutput, DotenvParseOutput } from 'dotenv'
import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { ILogger } from '../logger/logger.interface'

@injectable()
export class ConfigService implements IConfigService {
  private readonly config: DotenvParseOutput
  constructor(@inject(TYPES.Logger) private logger: ILogger) {
    const result: DotenvConfigOutput = config()

    if (result.error) {
      this.logger.error('Error loading .env file')
    } else {
      this.config = result.parsed as DotenvParseOutput
    }
  }

  get(key: string): string {
    return this.config[key]
  }
}
