import { App } from './app'
import { LoggerService } from './logger/logger.service'
import { UserController } from './user/user.controller'
import { ExceptionFilter } from './errors/exception.filter'
import { Container, ContainerModule, interfaces } from 'inversify'
import { TYPES } from './types'
import { ILogger } from './logger/logger.interface'
import { IExceptionFilter } from './errors/exception.filter.interface'
import { IUserController } from './user/user.controller.interface'
import { IUserService } from './user/user.service.interface'
import { UserService } from './user/user.service'
import { IConfigService } from './config/config.service.interface'
import { ConfigService } from './config/config.service'
import { PrismaService } from './database/prisma.service'
import { IUserRepository } from './user/user.repository.interface'
import { UserRepository } from './user/user.repository'

export interface IBootstrap {
  appContainer: Container
  app: App
}

export const appBindings = new ContainerModule((bind: interfaces.Bind) => {
  bind<ILogger>(TYPES.Logger).to(LoggerService).inSingletonScope()
  bind<IExceptionFilter>(TYPES.ExceptionFilter).to(ExceptionFilter).inSingletonScope()
  bind<IUserController>(TYPES.UserController).to(UserController).inSingletonScope()
  bind<IUserService>(TYPES.UserService).to(UserService).inSingletonScope()
  bind<IConfigService>(TYPES.ConfigService).to(ConfigService).inSingletonScope()
  bind<IUserRepository>(TYPES.UserRepository).to(UserRepository).inSingletonScope()
  bind<PrismaService>(TYPES.PrismaService).to(PrismaService).inSingletonScope()
  bind<App>(TYPES.Application).to(App).inSingletonScope()
})

function bootstrap(): IBootstrap {
  const appContainer = new Container()

  appContainer.load(appBindings)

  const app = appContainer.get<App>(TYPES.Application)

  app.init()

  return {
    app,
    appContainer,
  }
}

export const { app, appContainer } = bootstrap()
