export const TYPES = {
  Application: Symbol.for('Application'),
  Logger: Symbol.for('ILogger'),
  UserController: Symbol.for('IUserController'),
  UserService: Symbol.for('UserService'),
  ExceptionFilter: Symbol.for('IExceptionFilter'),
  ConfigService: Symbol.for('IConfigService'),
  PrismaService: Symbol.for('IPrismaService'),
  UserRepository: Symbol.for('UserRepository'),
}
