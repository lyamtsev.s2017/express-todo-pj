import { IsEmail, IsString, MinLength } from 'class-validator'

export class UserRegisterDto {
  @IsEmail({}, { message: 'Invalid email' })
  email: string

  @IsString({ message: 'Invalid password' })
  @MinLength(1, { message: 'Please enter password' })
  password: string

  @IsString({ message: 'Invalid name' })
  @MinLength(1, { message: 'Please enter name' })
  name: string
}
