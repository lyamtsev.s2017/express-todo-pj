import { BaseController } from '../common/base.controller'
import { NextFunction, Request, Response } from 'express'
import { HTTPError } from '../errors/http-error.class'
import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { ILogger } from '../logger/logger.interface'
import 'reflect-metadata'
import { IUserController } from './user.controller.interface'
import { UserLoginDto } from './dto/login'
import { UserRegisterDto } from './dto/register'
import { IUserService } from './user.service.interface'
import { ValidateMiddleware } from '../common/validate.middleware'
import { sign } from 'jsonwebtoken'
import { IConfigService } from '../config/config.service.interface'
import { AuthMiddleware } from '../common/auth.middleware'

@injectable()
export class UserController extends BaseController implements IUserController {
  constructor(
    @inject(TYPES.Logger) private loggerService: ILogger,
    @inject(TYPES.UserService) private userService: IUserService,
    @inject(TYPES.ConfigService) private configService: IConfigService,
  ) {
    super(loggerService)
    this.bindRoutes([
      {
        path: '/register',
        method: 'post',
        func: this.register,
        middlewares: [new ValidateMiddleware(UserRegisterDto)],
      },
      {
        path: '/login',
        method: 'post',
        func: this.login,
        middlewares: [new ValidateMiddleware(UserLoginDto)],
      },
      {
        path: '/info',
        method: 'get',
        func: this.info,
        middlewares: [new AuthMiddleware()],
      },
    ])
  }

  async login(
    req: Request<{}, {}, UserLoginDto>,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    const result = await this.userService.validateUser(req.body)

    if (!result) {
      return next(new HTTPError(401, 'Unauthorized', 'UserController.login'))
    }

    const secret = this.configService.get('JWT_SECRET')

    const jwt = await this.signJWT(req.body.email, secret)

    this.ok(res, { jwt })
  }

  async register(
    { body }: Request<{}, {}, UserRegisterDto>,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    const result = await this.userService.createUser(body)

    if (!result) {
      return next(new HTTPError(422, 'User already exists', 'UserController.register'))
    }

    this.ok(res, result)
  }

  async info({ user }: Request, res: Response, next: NextFunction): Promise<void> {
    const userInfo = await this.userService.getUserInfo(user)

    this.ok(res, { test: 123 })
  }

  private signJWT(email: string, secret: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      sign(
        { email, iat: Math.floor(Date.now() / 1000) },
        secret,
        { algorithm: 'HS256' },
        (err, token) => {
          if (err) {
            reject(err)
          }
          resolve(token as string)
        },
      )
    })
  }
}
