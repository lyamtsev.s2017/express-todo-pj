import { IUserService } from './user.service.interface'
import { UserRegisterDto } from './dto/register'
import { User } from './user.entity'
import { UserLoginDto } from './dto/login'
import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { IConfigService } from '../config/config.service.interface'
import { IUserRepository } from './user.repository.interface'
import { UserModel } from '@prisma/client'

@injectable()
export class UserService implements IUserService {
  constructor(
    @inject(TYPES.ConfigService) private configService: IConfigService,
    @inject(TYPES.UserRepository) private userRepository: IUserRepository,
  ) {}

  async createUser({ email, name, password }: UserRegisterDto): Promise<UserModel | null> {
    const newUser = new User(email, name)
    const salt = this.configService.get('SALT')
    await newUser.setPassword(password, +salt)

    const existingUser = await this.userRepository.find(email)

    if (existingUser) {
      return null
    }

    return this.userRepository.create(newUser)
  }

  async validateUser({ email, password }: UserLoginDto): Promise<boolean> {
    const existingUser = await this.userRepository.find(email)

    if (!existingUser) {
      return false
    }

    const newUser = new User(existingUser.email, existingUser.name, existingUser.password)

    return newUser.comparePassword(password)
  }

  async getUserInfo(email: string): Promise<UserModel | null> {
    return this.userRepository.find(email)
  }
}
