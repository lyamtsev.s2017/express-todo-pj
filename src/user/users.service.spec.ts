import 'reflect-metadata'
import { Container } from 'inversify'
import { IConfigService } from '../config/config.service.interface'
import { IUserRepository } from './user.repository.interface'
import { IUserService } from './user.service.interface'
import { TYPES } from '../types'
import { UserService } from './user.service'
import { User } from './user.entity'
import { UserModel } from '@prisma/client'

const ConfigServiceMock: IConfigService = {
  get: jest.fn(),
}

const UserRepositoryMock: IUserRepository = {
  find: jest.fn(),
  create: jest.fn(),
}

const container = new Container()

let configService: IConfigService
let userRepository: IUserRepository
let userService: IUserService

beforeAll(() => {
  container.bind<IUserService>(TYPES.UserService).to(UserService).inSingletonScope()
  container.bind<IConfigService>(TYPES.ConfigService).toConstantValue(ConfigServiceMock)
  container.bind<IUserRepository>(TYPES.UserRepository).toConstantValue(UserRepositoryMock)

  configService = container.get<IConfigService>(TYPES.ConfigService)
  userRepository = container.get<IUserRepository>(TYPES.UserRepository)
  userService = container.get<IUserService>(TYPES.UserService)
})

describe('User Service', () => {
  it('createUser - user creates successfully', async () => {
    configService.get = jest.fn().mockReturnValueOnce('1')
    userRepository.find = jest.fn().mockReturnValueOnce(null)
    userRepository.create = jest.fn().mockImplementationOnce((user: User): UserModel => {
      return {
        name: user.name,
        email: user.email,
        password: user.password,
        id: 1,
      }
    })

    const TEST_PASS = '1'

    const createdUser = await userService.createUser({
      email: 'a@a.ua',
      name: 'Test',
      password: TEST_PASS,
    })

    expect(createdUser?.password).not.toEqual(TEST_PASS)
  })

  it('createUser - user already exists', async () => {
    configService.get = jest.fn().mockReturnValueOnce('1')
    userRepository.find = jest.fn().mockImplementationOnce((email: string): UserModel => {
      return {
        name: 'Test',
        email,
        password: '1',
        id: 1,
      }
    })

    const TEST_PASS = '1'

    const createdUser = await userService.createUser({
      email: 'a@a.ua',
      name: 'Test',
      password: TEST_PASS,
    })

    expect(createdUser).toBe(null)
  })

  it('validateUser - user exists and password is correct', async () => {
    const TEST_PASS = '1'

    configService.get = jest.fn().mockReturnValueOnce('1')

    const newUser = new User('test@mail.com', 'Test')
    await newUser.setPassword(TEST_PASS, +configService.get('SALT'))

    userRepository.find = jest.fn().mockImplementationOnce((email: string): UserModel => {
      return {
        name: newUser.name,
        email: email,
        password: newUser.password,
        id: 1,
      }
    })

    const isValid = await userService.validateUser({ email: 'test@mail.com', password: TEST_PASS })

    expect(isValid).toBe(true)
  })

  it('validateUser - user exists and password is incorrect', async () => {
    const TEST_PASS = '1'

    configService.get = jest.fn().mockReturnValueOnce('1')

    const newUser = new User('test@mail.com', 'Test')
    await newUser.setPassword(TEST_PASS, +configService.get('SALT'))

    userRepository.find = jest.fn().mockImplementationOnce((email: string): UserModel => {
      return {
        name: newUser.name,
        email: email,
        password: newUser.password,
        id: 1,
      }
    })

    const isValid = await userService.validateUser({
      email: 'test@mail.com',
      password: 'INVALID_PASSWORD',
    })

    expect(isValid).toBe(false)
  })

  it('validateUser - user does not exist', async () => {
    userRepository.find = jest.fn().mockReturnValueOnce(null)

    const isValid = await userService.validateUser({ email: 'test@mail.com', password: '1' })

    expect(isValid).toBe(false)
  })

  it('getUserInfo - return a user', async () => {
    userRepository.find = jest.fn().mockImplementationOnce((email: string): UserModel => {
      return {
        name: 'Test',
        email,
        password: '1',
        id: 1,
      }
    })

    const user = await userService.getUserInfo('test@mail.com')

    expect(user).not.toBe(null)
  })
})
